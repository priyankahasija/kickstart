#!/bin/bash
#
#
# For the creation of unattended iso 
#
#----------------------------------
# SETTING VARIABLES
#----------------------------------

ISO_FILE_PATH=$1
KICKSTART_FILE_PATH=$2
PRESEED_FILE_PATH=$3

#----------------------------------------
# MOUNT THE ISO AND SET RIGHT PERMISSIONS
#----------------------------------------

mkdir ubuntu_iso
sudo mount -r -o loop $ISO_FILE_PATH ubuntu_iso
mkdir ubuntu_files
rsync -a ubuntu_iso/ ubuntu_files/
chown msys:msys ubuntu_files
chmod 755 ubuntu_files
umount ubuntu_iso
rm -rf ubuntu_iso

#----------------------------------
# EDITING THE CONTENTS OF ISO IMAGE
#----------------------------------

cp {$KICKSTART_FILE_PATH,$PRESEED_FILE_PATH} ubuntu_files
chmod 644 ubuntu_files/*.cfg ubuntu_files/*.seed
chmod 755 ubuntu_files/isolinux ubuntu_files/isolinux/txt.cfg ubuntu_files/isolinux/isolinux.cfg
sed -i '/^\default install$/a label autoinstall\n\ \ menu label ^Automatically install Ubuntu\n\ \ kernel /install/vmlinuz\n\ \ append file=/cdrom/preseed/ubuntu-server.seed vga=788 initrd=/install/initrd.gz ks=cdrom:/ks.cfg preseed/file=/cdrom/ubuntu-auto.seed quiet --' ubuntu_files/isolinux/txt.cfg
chmod 555 ubuntu_files/isolinux
chmod 444 ubuntu_files/isolinux/txt.cfg ubuntu_files/isolinux/isolinux.cfg

#-----------------------------------------------------
# CREATING NEW ISO IMAGE IN PRESENT WORKING DIRECTORY
#-----------------------------------------------------

mkisofs -D -r -V "ubuntu-auto" -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset utf-8 -cache-inodes -quiet -o ubuntu-auto.iso ubuntu_files/

#--------
#CLEANUP
#--------

rm -rf ubuntu_files




